package cz.svetonaut.springblog.security.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.security.access.prepost.PreAuthorize;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD, ElementType.TYPE})
@PreAuthorize("hasRole('ROLE_ADMIN') or @userRepository.findByUsername(principal.getUsername()).userId == #userId")
public @interface AdminOrSameUser {
}
