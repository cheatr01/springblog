package cz.svetonaut.springblog.security;

import cz.svetonaut.springblog.model.entity.User;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Spring security dto.
 */
public class BlogUserDetails implements UserDetails {

    private final User user;

    public BlogUserDetails(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> auths = new ArrayList<>();
        final String[] roles = user.getRoles().split(", ");
        for (String role : roles) {
            auths.add(new SimpleGrantedAuthority(role));
        }
        return auths;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return !user.getDeleted();
    }

    @Override
    public boolean isAccountNonLocked() {
        return !user.getDeleted();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !user.getDeleted();
    }

    @Override
    public boolean isEnabled() {
        return !user.getDeleted();
    }
}
