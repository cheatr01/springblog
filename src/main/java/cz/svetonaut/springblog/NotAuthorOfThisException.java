package cz.svetonaut.springblog;

public class NotAuthorOfThisException extends RuntimeException {

    public NotAuthorOfThisException() {
    }

    public NotAuthorOfThisException(String message) {
        super(message);
    }

    public NotAuthorOfThisException(String message, Throwable cause) {
        super(message, cause);
    }
}
