package cz.svetonaut.springblog;

public class NonUniqueUsernameException extends RuntimeException {

    public static final String MESSAGE_TEMPLATE = "Username %s is not Unique";

    public NonUniqueUsernameException(String username) {
        super(String.format(MESSAGE_TEMPLATE, username));
    }
}
