package cz.svetonaut.springblog.service;

import cz.svetonaut.springblog.NotAuthorOfThisException;
import cz.svetonaut.springblog.model.dto.ArticleDto;
import cz.svetonaut.springblog.model.dto.ArticleView;
import cz.svetonaut.springblog.model.entity.Article;
import cz.svetonaut.springblog.model.entity.User;
import cz.svetonaut.springblog.repository.ArticleRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Service layer for Article.
 */
@Service
@Transactional
@RequiredArgsConstructor
public class ArticleService {

    private final ArticleRepository articleRepository;
    private final ModelMapper modelMapper;

    public List<ArticleView> findAllVisible() {
        return articleRepository.findAllByVisibly(true);
    }

    public ArticleView findByIdVisible(Long articleId) {
        ArticleView article = articleRepository.findByArticleIdAndVisibly(articleId, true);
        if (article == null) {
            throw new EntityNotFoundException("Not found Article for id: " + articleId);
        }
        return article;
    }

    public ArticleDto saveArticle(ArticleDto dto) {
        if (dto == null) {
            throw new IllegalArgumentException("Article does not be null!");
        }
        Article entity = new Article();
        modelMapper.map(dto, entity);
        Article saved = articleRepository.save(entity);
        modelMapper.map(saved, dto);
        return dto;
    }

    public ArticleDto updateArticle(Authentication principal, ArticleDto dto) {
        if (dto == null) {
            throw new IllegalArgumentException("Article does not be null!");
        }
        if (isAuthorOfThisArticle(principal, dto.getArticleId()) || isAdmin(principal)) {
            return saveArticle(dto);
        } else {
            throw new NotAuthorOfThisException("Authors are can update only theirs articles");
        }
    }


    public void deleteArticle(Long articleId) {
        articleRepository.deleteById(articleId);
    }

    private boolean isAdmin(Authentication principal) {
        final Collection<? extends GrantedAuthority> authorities = principal.getAuthorities();
        for (var authority : authorities) {
            if (authority.getAuthority().equals("ROLE_ADMIN")) {
                return true;
            }
        }
        return false;
    }

    private boolean isAuthorOfThisArticle(Authentication principal, Long articleId) {
        if (principal != null && articleId != null) {
            final Optional<Article> foundArticle = articleRepository.findById(articleId);
            return foundArticle.map(Article::getAuthor).map(User::getUsername).orElse("").equals(principal.getName());
        } else {
            return false;
        }
    }

    public void deleteAllArticlesOfUser(Long userId) {
        articleRepository.deleteArticlesByAuthor_UserId(userId);
    }
}
