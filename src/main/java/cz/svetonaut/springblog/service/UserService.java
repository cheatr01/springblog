package cz.svetonaut.springblog.service;

import cz.svetonaut.springblog.NonUniqueUsernameException;
import cz.svetonaut.springblog.model.dto.UserDto;
import cz.svetonaut.springblog.model.dto.UserView;
import cz.svetonaut.springblog.model.entity.User;
import cz.svetonaut.springblog.repository.UserRepository;
import cz.svetonaut.springblog.util.PasswordChecker;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final ArticleService articleService;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder passwordEncoder;
    private final PasswordChecker passwordChecker;

    public UserView findUserById(Long userId) {
        return userRepository.getByUserId(userId);
    }

    public List<UserView> findAllUsers() {
        return userRepository.findAllUsers();
    }

    public UserDto registerNewUser(UserDto userDto) {
        checkForUsernameUniqueness(userDto.getUsername());
        passwordChecker.checkNewPassword(userDto.getPassword());

        User mapped = modelMapper.map(userDto, User.class);
        mapped.setPassword(passwordEncoder.encode(mapped.getPassword()));
        mapped.setRoles("ROLE_USER");

        final User saved = userRepository.save(mapped);
        modelMapper.map(saved, userDto);
        userDto.setPassword(null); //todo find smooth way

        return userDto;
    }

    private void checkForUsernameUniqueness(String username) {
        final User byUsername = userRepository.findByUsername(username);
        if (byUsername != null) {
            throw new NonUniqueUsernameException(username);
        }
    }

    public UserDto updateUser(UserDto userDto) {
        final User user = userRepository.findById(userDto.getUserId()).orElseThrow();
        modelMapper.map(userDto, user);
        final User saved = userRepository.save(user);
        modelMapper.map(saved, userDto);
        userDto.setPassword(null); //todo find smooth way
        return userDto;
    }

    public void deleteUser(Long userId) {
        articleService.deleteAllArticlesOfUser(userId);
        userRepository.deleteById(userId);
    }
}
