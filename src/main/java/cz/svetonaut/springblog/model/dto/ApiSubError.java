package cz.svetonaut.springblog.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto for communication about errors to client od our REST resources.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiSubError {

    private String object;
    private String field;
    private String rejectedValue;
    private String message;

}
