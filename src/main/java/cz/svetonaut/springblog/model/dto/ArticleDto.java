package cz.svetonaut.springblog.model.dto;

import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * Dto for json communication from client to us. Use for POST and PUT request.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleDto {

    @NotNull(groups = PutMapping.class)
    private Long articleId;
    private Date createDate;
    @Size(max = 255, groups = {Default.class, PutMapping.class})
    private String description;
    @NotBlank
    private String text;
    @NotBlank(groups = {Default.class, PutMapping.class})
    @NotNull(groups = {Default.class, PutMapping.class})
    @Size(max = 255, groups = {Default.class, PutMapping.class})
    private String title;
    private Boolean visibly;
    @NotNull(groups = {Default.class, PutMapping.class})
    private UserDto author;

}
