package cz.svetonaut.springblog.model.dto;

import java.util.Date;

/**
 * SpringData view for json communication from us to client. Use for GET request.
 */
public interface UserView {

    Long getUserId();

    void setUserId(Long userId);

    Date getCreatedAt();

    void setCreatedAt(Date createdAt);

    Boolean getDeleted();

    void setDeleted(Boolean deleted);

    String getEmail();

    void setEmail(String email);

    Date getLastLogged();

    void setLastLogged(Date lastLogged);

    String getUsername();

    void setUsername(String username);

    String getRoles();

    void setRoles(String roles);
}
