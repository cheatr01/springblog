package cz.svetonaut.springblog.model.dto;

import java.util.Date;

/**
 * SpringData view for json communication from us to client. Use for GET request.
 * It's nested with UserView.
 */
public interface ArticleView {

    Long getArticleId();

    void setArticleId(Long articleId);

    Date getCreateDate();

    void setCreateDate(Date createDate);

    String getDescription();

    void setDescription(String description);

    String getText();

    void setText(String text);

    String getTitle();

    void setTitle(String title);

    Boolean getVisibly();

    void setVisibly(Boolean visibly);

    UserView getAuthor();

    void setAuthor(UserView authorId);
}
