package cz.svetonaut.springblog.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolation;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * Dto for communication about errors to client od our REST resources.
 */
@Data
public class ApiError {

    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String message;
    private String debugMessage;
    private List<ApiSubError> subErrors = new ArrayList<>();

    private ApiError() {
        timestamp = LocalDateTime.now();
    }

    public ApiError(HttpStatus status) {
        this();
        this.status = status;
    }

    public ApiError(HttpStatus status, Throwable ex) {
        this();
        this.status = status;
        this.message = "Unexpected error";
        this.debugMessage = ex.getLocalizedMessage();
    }

    public ApiError(HttpStatus status, String message, Throwable ex) {
        this();
        this.status = status;
        this.message = message;
        this.debugMessage = ex.getLocalizedMessage();
    }

    public void addSubError(ConstraintViolation<?> violation) {
        ApiSubError subError = new ApiSubError();
        subError.setObject(violation.getLeafBean().toString());
        subError.setField(violation.getPropertyPath().toString());
        subError.setRejectedValue(violation.getInvalidValue().toString());
        subError.setMessage(violation.getMessage());
        subErrors.add(subError);
    }

    public void addSubError(ApiSubError subError) {
        subErrors.add(subError);
    }
}
