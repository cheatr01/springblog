package cz.svetonaut.springblog.model.dto;

import java.util.Date;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * Dto for json communication from client to us. Use for POST and PUT request.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    @NotNull(groups = PutMapping.class)
    @Null(groups = PostMapping.class)
    private Long userId;
    private Date createdAt;
    private Boolean deleted;
    @NotNull(groups = PostMapping.class)
    @Email(groups = {PostMapping.class, PutMapping.class})
    private String email;
    private Date lastLogged;
    private String password;
    @NotNull(groups = {PostMapping.class, PutMapping.class})
    @NotBlank(groups = {PostMapping.class, PutMapping.class})
    private String username;
    private String roles;

    public UserDto(Long userId) {
        this.userId = userId;
    }

}
