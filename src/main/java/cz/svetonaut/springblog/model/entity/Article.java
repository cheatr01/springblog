package cz.svetonaut.springblog.model.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "article")
public class Article {
    @Id
    @Column(name = "article_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    @ToString.Include
    private Long articleId;

    @Basic
    @Column(name = "create_date", nullable = false)
    @CreationTimestamp
    private Date createDate;

    @Basic
    @Column(name = "update_date")
    @UpdateTimestamp
    private Date updateDate;

    @Basic
    @Column(name = "description", nullable = true, length = 255)
    private String description;

    @Basic
    @Column(name = "text", nullable = false)
    private String text;

    @Basic
    @Column(name = "title", nullable = false, length = 255)
    @EqualsAndHashCode.Include
    @ToString.Include
    @NonNull
    private String title;

    @Basic
    @Column(name = "visibly", nullable = false)
    @NonNull
    private Boolean visibly = true;

    @ManyToOne/*(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.DETACH})*/
    @JoinColumn(name = "author_id", referencedColumnName = "user_id", nullable = false)
    @EqualsAndHashCode.Include
    @NonNull
    private User author;

}
