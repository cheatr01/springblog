package cz.svetonaut.springblog.model.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @Column(name = "user_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    @ToString.Include
    private Long userId;

    @Basic
    @Column(name = "created_at", nullable = true)
    @CreationTimestamp
    private Date createdAt;

    @Basic
    @Column(name = "update_date")
    @UpdateTimestamp
    private Date updateDate;

    @Basic
    @Column(name = "deleted", nullable = false)
    @NonNull
    private Boolean deleted = false;

    @Basic
    @Column(name = "email", nullable = false, length = 55)
    @EqualsAndHashCode.Include
    @ToString.Include
    @NonNull
    private String email;

    @Basic
    @Column(name = "last_logged", nullable = true)
    private Date lastLogged;

    @Basic
    @Column(name = "password", nullable = false, length = 150)
    @NonNull
    private String password;

    @Basic
    @Column(name = "username", nullable = false)
    @EqualsAndHashCode.Include
    @ToString.Include
    @NonNull
    private String username;

    @Basic
    @Column(name = "roles", nullable = false)
    @EqualsAndHashCode.Include
    @ToString.Include
    @NonNull
    private String roles = "ROLE_USER"; //todo enumeration
}
