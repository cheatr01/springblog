package cz.svetonaut.springblog;

public class NonSufficiencyPasswordException extends RuntimeException {

    public static final String MESSAGE_TEMPLATE = "Password must be minimal %d characters lenght and must contains numbers";

    public NonSufficiencyPasswordException(int lengthOfPassword) {
        super(String.format(MESSAGE_TEMPLATE, lengthOfPassword));
    }
}
