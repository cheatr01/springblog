package cz.svetonaut.springblog.util;

import cz.svetonaut.springblog.NonSufficiencyPasswordException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PasswordChecker {

    @Value("${cz.svetonaut.springblog.password.length:8}")
    private Integer customizedLengthOfPassword;

    public void checkNewPassword(String password) {
        int length = customizedLengthOfPassword;
        if (password == null
                || password.length() < length
                || !password.matches(".*\\d.*")) {
            throw new NonSufficiencyPasswordException(customizedLengthOfPassword);
        }
    }
}
