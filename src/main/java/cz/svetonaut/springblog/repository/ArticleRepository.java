package cz.svetonaut.springblog.repository;

import cz.svetonaut.springblog.model.dto.ArticleView;
import cz.svetonaut.springblog.model.entity.Article;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

    List<ArticleView> findAllByVisibly(boolean isVisibly);

    ArticleView findByArticleIdAndVisibly(Long articleId, boolean visibly);

    void deleteArticlesByAuthor_UserId(Long userId);
}
