package cz.svetonaut.springblog.repository;

import cz.svetonaut.springblog.model.dto.UserView;
import cz.svetonaut.springblog.model.entity.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    UserView getByUserId(Long userId);

    @Query("select u from User u")
    List<UserView> findAllUsers();
}
