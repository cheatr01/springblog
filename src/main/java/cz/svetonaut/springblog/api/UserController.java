package cz.svetonaut.springblog.api;

import cz.svetonaut.springblog.model.dto.UserDto;
import cz.svetonaut.springblog.model.dto.UserView;
import cz.svetonaut.springblog.security.annotation.AdminOrSameUser;
import cz.svetonaut.springblog.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/user")
@Scope("request")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping(path = "/{userId}")
    @AdminOrSameUser
    public UserView getUserById(@PathVariable Long userId) {
        return userService.findUserById(userId);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<UserView> getAllUsers() {
        return userService.findAllUsers();
    }

    @PostMapping
    public UserDto registerNewUser(@Validated(PostMapping.class) @RequestBody UserDto userDto) {
        return userService.registerNewUser(userDto);
    }

    @PutMapping(path = "/{userId}")
    @AdminOrSameUser
    public UserDto updateUser(@Validated(PutMapping.class) @RequestBody UserDto userDto, @PathVariable Long userId) {
        return userService.updateUser(userDto);
    }

    @DeleteMapping(path = "/{userId}")
    @AdminOrSameUser
    public ResponseEntity<Object> deleteUser(@PathVariable Long userId) {
        userService.deleteUser(userId);
        return new ResponseEntity<>(HttpStatus.OK); //RestExceptionHandler handle errors from repository

    }
}
