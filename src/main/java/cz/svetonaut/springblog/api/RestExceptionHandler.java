package cz.svetonaut.springblog.api;

import cz.svetonaut.springblog.NonSufficiencyPasswordException;
import cz.svetonaut.springblog.NonUniqueUsernameException;
import cz.svetonaut.springblog.NotAuthorOfThisException;
import cz.svetonaut.springblog.model.dto.ApiError;
import java.util.Set;
import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handleNotFoundException(EntityNotFoundException e) {
        ApiError error = new ApiError(HttpStatus.NOT_FOUND, e.getMessage(), e);
        return new ResponseEntity<Object>(error, error.getStatus());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException e) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        return new ResponseEntity<Object>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleValidationException(ConstraintViolationException e) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
        for (ConstraintViolation<?> violation : constraintViolations) {
            apiError.addSubError(violation);
        }
        return new ResponseEntity<Object>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<Object> handleEmptyResultException(EmptyResultDataAccessException e) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "Entity with your id doesn't exist", e);
        return new ResponseEntity<Object>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(NotAuthorOfThisException.class)
    public ResponseEntity<Object> handleNotAuthorException(NotAuthorOfThisException e) {
        ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, e.getMessage(), e);
        return new ResponseEntity<Object>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(NonUniqueUsernameException.class)
    public ResponseEntity<Object> handleNonUniqueUsernameException(NonUniqueUsernameException e) {
        final ApiError error = new ApiError(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        return new ResponseEntity<>(error, error.getStatus());
    }

    @ExceptionHandler(NonSufficiencyPasswordException.class)
    public ResponseEntity<Object> handleNonSufficiencyPasswordException(NonSufficiencyPasswordException e) {
        final ApiError error = new ApiError(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        return new ResponseEntity<>(error, error.getStatus());
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "Validation errors for parametr " + e.getParameter().getParameterType() + " " + e.getParameter().getParameterName(), e);
        return new ResponseEntity<Object>(apiError, apiError.getStatus());
    }
}
