package cz.svetonaut.springblog.api;

import cz.svetonaut.springblog.model.dto.ArticleDto;
import cz.svetonaut.springblog.model.dto.ArticleView;
import cz.svetonaut.springblog.service.ArticleService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/article")
@Scope("request")
@RequiredArgsConstructor
public class ArticleController {

    private final ArticleService articleService;

    @GetMapping
    public List<ArticleView> getVisiblyArticles() {
        return articleService.findAllVisible();
    }

    @GetMapping(path = "/{articleId}")
    public ArticleView getVisiblyArticleById(@PathVariable Long articleId) {
        return articleService.findByIdVisible(articleId);
    }

    @PostMapping
    @PostAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_AUTHOR')")
    public ArticleDto saveNewArticle(@Valid @RequestBody ArticleDto dto) {
        return articleService.saveArticle(dto);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_AUTHOR')")
    public ArticleDto updateArticle(@Validated(PutMapping.class) @RequestBody ArticleDto dto, Authentication principal) {
        return articleService.updateArticle(principal, dto);
    }

    @DeleteMapping(path = "/{articleId}")
    public ResponseEntity<Object> deleteArticle(@PathVariable Long articleId) {
        articleService.deleteArticle(articleId);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }
}
