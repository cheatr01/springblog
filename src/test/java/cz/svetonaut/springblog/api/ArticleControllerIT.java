package cz.svetonaut.springblog.api;

import com.jayway.jsonpath.JsonPath;
import cz.svetonaut.springblog.SpringblogApplication;
import cz.svetonaut.springblog.model.dto.ArticleDto;
import cz.svetonaut.springblog.model.dto.UserDto;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

/**
 * Integration test of REST inteface with full in memory db and security.
 * Is use H2 db, which is populated bu data.sql script in app resources.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {SpringblogApplication.class})
@AutoConfigureTestDatabase
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ArticleControllerIT {

    @LocalServerPort
    private Integer port;
    private final RestTemplate restTemplate = new RestTemplate();
    private final HttpHeaders httpHeaders = new HttpHeaders();

    @BeforeEach
    void setUp() {
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            protected boolean hasError(HttpStatus statusCode) {
                return false;
            }
        });
        httpHeaders.setBasicAuth("totok", "simitar");
    }

    @Test
    @Order(10)
    void testGetVisiblyArticles() {
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.GET, entity, String.class);
        assertThat(response.getStatusCode().value()).isEqualTo(200);
        assertThat(JsonPath.parse(response.getBody()).read("$.length()", Integer.class)).isEqualTo(5);
    }

    @Test
    @Order(20)
    void testGetVisiblyArticleByIdOnNonExistingArticle() {
        int id = 666;
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/" + id, HttpMethod.GET, entity, String.class);
        assertThat(response.getStatusCode().value()).isEqualTo(404);
        assertThat(JsonPath.parse(response.getBody()).read("$.message", String.class)).isEqualTo("Not found Article for id: " + id);
    }

    @Test
    @Order(30)
    void getVisiblyArticleById() {
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/6", HttpMethod.GET, entity, String.class);
        assertThat(response.getStatusCode().value()).isEqualTo(200);
        assertThat(JsonPath.parse(response.getBody()).read("$.articleId", Long.class)).isEqualTo(6);
        assertThat(JsonPath.parse(response.getBody()).read("$.title", String.class)).isEqualTo("Pokusíček");
    }

    @Test
    @Order(40)
    void saveNewArticle() {
        String fromIntegrationTest = "From integration test";
        ArticleDto articleDto = getPreparedArticleDto(fromIntegrationTest);
        setAuthWithAuthorUser();
        HttpEntity<ArticleDto> entity = new HttpEntity<>(articleDto, httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, entity, String.class);
        assertThat(response.getStatusCode().value()).isEqualTo(200);
        assertThat(JsonPath.parse(response.getBody()).read("$.title", String.class)).isEqualTo(fromIntegrationTest);
        assertThat(JsonPath.parse(response.getBody()).read("$.createDate", String.class)).isNotBlank();  //test added default value
        assertThat(JsonPath.parse(response.getBody()).read("$.visibly", Boolean.class)).isTrue(); //test added default value
    }

    @Test
    @Order(45)
    void saveNewArticleWithWrongRole() {
        String fromIntegrationTest = "With wrong role";
        ArticleDto articleDto = getPreparedArticleDto(fromIntegrationTest);
        HttpEntity<ArticleDto> entity = new HttpEntity<>(articleDto, httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, entity, String.class);
        assertThat(response.getStatusCode().value()).isEqualTo(403);
    }

    @Test
    @Order(50)
    void saveNewArticleWithoutTitle() {
        ArticleDto articleDto = getPreparedArticleDto(null);
        HttpEntity<ArticleDto> entity = new HttpEntity<>(articleDto, httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, entity, String.class);
        assertThat(response.getStatusCode().value()).isEqualTo(400);
        assertThat(JsonPath.parse(response.getBody()).read("$.message", String.class)).isEqualTo("Validation errors for parametr class cz.svetonaut.springblog.model.dto.ArticleDto dto");
    }

    @Test
    @Order(60)
    void saveNewArticleWithBlankTitle() {
        ArticleDto articleDto = getPreparedArticleDto("\t");
        HttpEntity<ArticleDto> entity = new HttpEntity<>(articleDto, httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, entity, String.class);
        assertThat(response.getStatusCode().value()).isEqualTo(400);
        assertThat(JsonPath.parse(response.getBody()).read("$.message", String.class)).isEqualTo("Validation errors for parametr class cz.svetonaut.springblog.model.dto.ArticleDto dto");
    }

    @Test
    @Order(70)
    void updateArticle() {
        var newTitle = "Updated title";
        setAuthWithAuthorUser();
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        ResponseEntity<ArticleDto> response = restTemplate.exchange(getUrl() + "/7", HttpMethod.GET, entity, ArticleDto.class);
        final ArticleDto dto = response.getBody();
        Optional.ofNullable(dto).ifPresent(d -> d.setTitle(newTitle));
        final HttpEntity<ArticleDto> updateEntity = new HttpEntity<>(dto, httpHeaders);
        final ResponseEntity<String> exchange = restTemplate.exchange(getUrl(), HttpMethod.PUT, updateEntity, String.class);
        assertThat(exchange.getStatusCode().value()).isEqualTo(200);
        assertThat(JsonPath.parse(exchange.getBody()).read("$.articleId", Long.class)).isEqualTo(7);
        assertThat(JsonPath.parse(exchange.getBody()).read("$.title", String.class)).isEqualTo(newTitle);
    }

    @Test
    @Order(71)
    void updateArticleWithUserRole() {
        var newTitle = "Updated title with wrong role";
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        ResponseEntity<ArticleDto> response = restTemplate.exchange(getUrl() + "/7", HttpMethod.GET, entity, ArticleDto.class);
        final ArticleDto dto = response.getBody();
        Optional.ofNullable(dto).ifPresent(d -> d.setTitle(newTitle));
        final HttpEntity<ArticleDto> updateEntity = new HttpEntity<>(dto, httpHeaders);
        final ResponseEntity<String> exchange = restTemplate.exchange(getUrl(), HttpMethod.PUT, updateEntity, String.class);
        assertThat(exchange.getStatusCode().value()).isEqualTo(403);
    }

    @Test
    @Order(72)
    void updateArticleFromWrongAuthor() {
        var newTitle = "Updated title with wrong author";
        httpHeaders.setBasicAuth("stranger", "simitar");
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        ResponseEntity<ArticleDto> response = restTemplate.exchange(getUrl() + "/7", HttpMethod.GET, entity, ArticleDto.class);
        final ArticleDto dto = response.getBody();
        Optional.ofNullable(dto).ifPresent(d -> d.setTitle(newTitle));
        final HttpEntity<ArticleDto> updateEntity = new HttpEntity<>(dto, httpHeaders);
        final ResponseEntity<String> exchange = restTemplate.exchange(getUrl(), HttpMethod.PUT, updateEntity, String.class);
        assertThat(exchange.getStatusCode().value()).isEqualTo(403);
    }

    @Test
    @Order(73)
    void updateArticleByAdmin() {
        var newTitle = "Updated title by admin";
        httpHeaders.setBasicAuth("admin", "simitar");
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        ResponseEntity<ArticleDto> response = restTemplate.exchange(getUrl() + "/7", HttpMethod.GET, entity, ArticleDto.class);
        final ArticleDto dto = response.getBody();
        Optional.ofNullable(dto).ifPresent(d -> d.setTitle(newTitle));
        final HttpEntity<ArticleDto> updateEntity = new HttpEntity<>(dto, httpHeaders);
        final ResponseEntity<String> exchange = restTemplate.exchange(getUrl(), HttpMethod.PUT, updateEntity, String.class);
        assertThat(exchange.getStatusCode().value()).isEqualTo(200);
        assertThat(JsonPath.parse(exchange.getBody()).read("$.articleId", Long.class)).isEqualTo(7);
        assertThat(JsonPath.parse(exchange.getBody()).read("$.title", String.class)).isEqualTo(newTitle);
    }

    @Test
    @Order(80)
    void updateArticleWithoutId() {
        var newTitle = "Updated title";
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        ResponseEntity<ArticleDto> response = restTemplate.exchange(getUrl() + "/7", HttpMethod.GET, entity, ArticleDto.class);
        final ArticleDto dto = response.getBody();
        Optional.ofNullable(dto).ifPresent(d -> {
            d.setTitle(newTitle);
            d.setArticleId(null);
        });
        final HttpEntity<ArticleDto> updateEntity = new HttpEntity<>(dto, httpHeaders);
        final ResponseEntity<String> exchange = restTemplate.exchange(getUrl(), HttpMethod.PUT, updateEntity, String.class);
        assertThat(exchange.getStatusCode().value()).isEqualTo(400);
    }

    @Test
    @Order(90)
    void updateArticleWithBlankTitle() {
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        ResponseEntity<ArticleDto> response = restTemplate.exchange(getUrl() + "/7", HttpMethod.GET, entity, ArticleDto.class);
        final ArticleDto dto = response.getBody();
        Optional.ofNullable(dto).ifPresent(d -> d.setTitle(" "));
        final HttpEntity<ArticleDto> updateEntity = new HttpEntity<>(dto, httpHeaders);
        final ResponseEntity<String> exchange = restTemplate.exchange(getUrl(), HttpMethod.PUT, updateEntity, String.class);
        assertThat(exchange.getStatusCode().value()).isEqualTo(400);
        assertThat(JsonPath.parse(exchange.getBody()).read("$.message", String.class)).isEqualTo("Validation errors for parametr class cz.svetonaut.springblog.model.dto.ArticleDto dto");
    }

    @Test
    @Order(100)
    void deleteArticle() {
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/7", HttpMethod.DELETE, entity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    @Order(110)
    void deleteArticleWithNonExistingId() {
        Long id = 666L;
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/" + id, HttpMethod.DELETE, entity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
        assertThat(JsonPath.parse(response.getBody()).read("$.message", String.class)).isEqualTo("Entity with your id doesn't exist");
    }

    private void setAuthWithAuthorUser() {
        httpHeaders.setBasicAuth("potok", "simitar");
    }

    private String getUrl() {
        return "http://localhost:" + port + "/article";
    }

    private ArticleDto getPreparedArticleDto(String title) {
        ArticleDto articleDto = new ArticleDto();
        articleDto.setAuthor(new UserDto(2L));
        articleDto.setTitle(title);
        articleDto.setText("First article add from integration test");
        return articleDto;
    }
}