package cz.svetonaut.springblog.api;

import com.jayway.jsonpath.JsonPath;
import cz.svetonaut.springblog.model.dto.UserDto;
import java.io.IOException;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.fail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

/**
 * Integration test for UserController, data are in data.sql script and running on H2 embedded DB
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserControllerIT {

    private static final String TEST_PASSWORD = "1simitar1simitar";
    private static final String TEST_PASSWORD_ENCODED = "$2a$10$zJVB8xwl/jaXd7t8vldPgOloHUsa.940Gzbt4AMZUWfme0IPn3ciy";

    @Value("${cz.svetonaut.springblog.password.length:8}")
    private int passwordLength;

    @LocalServerPort
    private Long port;
    private RestTemplate restTemplate = new RestTemplate();
    private HttpHeaders headers = new HttpHeaders();

    @BeforeEach
    void setUp() {
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return false;
            }
        });
        headers.setBasicAuth("totok", "simitar");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @Order(10)
    void getUserById() {
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/1", HttpMethod.GET, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(JsonPath.parse(response.getBody()).read("$.username", String.class)).isEqualTo("totok");
    }

    @Test
    @Order(20)
    void getUserByIdByAdmin() {
        headers.setBasicAuth("admin", "simitar");
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/1", HttpMethod.GET, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(JsonPath.parse(response.getBody()).read("$.username", String.class)).isEqualTo("totok");
    }

    @Test
    @Order(30)
    void getUserByIdByWrongUser() {
        headers.setBasicAuth("potok", "simiatr");
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/1", HttpMethod.GET, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(401);
    }

    @Test
    @Order(40)
    void getAllUsers() {
        headers.setBasicAuth("admin", "simitar");
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.GET, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(JsonPath.parse(response.getBody()).read("$.length()", Integer.class)).isEqualTo(4);
    }

    @Test
    @Order(50)
    void getAllUsersByNotAdminUser() {
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.GET, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(403);
    }

    @Test
    @Order(60)
    void registerNewUser() {
        headers.clear();
        final HttpEntity<UserDto> httpEntity = new HttpEntity<>(prepareUserDto(), headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(JsonPath.parse(response.getBody()).read("$.password", String.class)).isNull();
        assertThat(JsonPath.parse(response.getBody()).read("$.username", String.class)).isEqualTo("tester");
        assertThat(JsonPath.parse(response.getBody()).read("$.deleted", Boolean.class)).isFalse();
//        assertThat(JsonPath.parse(response.getBody()).read("$.createdAt", Date.class)).isInSameDayAs(new Date()); // TODO předělat entity a dto do LocalDateTime
        assertThat(JsonPath.parse(response.getBody()).read("$.userId", Long.class)).isNotNull();
    }

    @Test
    @Order(70)
    void registerUserWithSameUsername() {
        headers.clear();
        final HttpEntity<UserDto> httpEntity = new HttpEntity<>(prepareUserDto(), headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
        assertThat(JsonPath.parse(response.getBody()).read("$.message", String.class)).isEqualTo("Username tester is not Unique");
    }

    @Test
    @Order(80)
    void registerUserWithShortPassword() {
        headers.clear();
        final UserDto dto = prepareUserDto();
        dto.setUsername("tester2");
        dto.setPassword("simitar");
        final HttpEntity<UserDto> httpEntity = new HttpEntity<>(dto, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
        assertThat(JsonPath.parse(response.getBody()).read("$.message", String.class)).isEqualTo(String.format("Password must be minimal %d characters lenght and must contains numbers", passwordLength));
    }

    @Test
    @Order(90)
    void registerUserWithoutUsername() {
        headers.clear();
        final UserDto dto = prepareUserDto();
        dto.setUsername(null);
        final HttpEntity<UserDto> httpEntity = new HttpEntity<>(dto, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(95)
    void registerUserWithBlankUsername() {
        headers.clear();
        final UserDto dto = prepareUserDto();
        dto.setUsername("");
        final HttpEntity<UserDto> httpEntity = new HttpEntity<>(dto, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(100)
    void registerUserWithId() {
        headers.clear();
        final UserDto dto = prepareUserDto();
        dto.setUserId(1L);
        final HttpEntity<UserDto> httpEntity = new HttpEntity<>(dto, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(110)
    void registerUserWithoutEmail() {
        headers.clear();
        final UserDto dto = prepareUserDto();
        dto.setUsername(null);
        final HttpEntity<UserDto> httpEntity = new HttpEntity<>(dto, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(120)
    void registerUserWithNotEmailString() {
        headers.clear();
        final UserDto dto = prepareUserDto();
        dto.setEmail("shbfshbsdbhfjsdf");
        final HttpEntity<UserDto> httpEntity = new HttpEntity<>(dto, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl(), HttpMethod.POST, httpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(130)
    void updateUser() {
        headers.setBasicAuth("tester", TEST_PASSWORD);
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> entity = restTemplate.exchange(getUrl() + "/5", HttpMethod.GET, httpEntity, UserDto.class);
        final UserDto prepared = entity.getBody();
        final String newEmail = "MrUpdated@hu.hu";

        if (prepared == null) {
            fail("Non prepared user in DB");
        }

        prepared.setEmail(newEmail);
        final HttpEntity<UserDto> putHttpEntity = new HttpEntity<>(prepared, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/5", HttpMethod.PUT, putHttpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(JsonPath.parse(response.getBody()).read("$.email", String.class)).isEqualTo(newEmail);
        assertThat(JsonPath.parse(response.getBody()).read("$.password", String.class)).isNull();
    }

    @Test
    @Order(140)
    void updateUserByAdmin() {
        headers.setBasicAuth("admin", "simitar");
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> entity = restTemplate.exchange(getUrl() + "/5", HttpMethod.GET, httpEntity, UserDto.class);
        final UserDto prepared = entity.getBody();
        final String newEmail = "MrUpdated2@hu.hu";

        if (prepared == null) {
            fail("Non prepared user in DB");
        }

        prepared.setEmail(newEmail);
        final HttpEntity<UserDto> putHttpEntity = new HttpEntity<>(prepared, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/5", HttpMethod.PUT, putHttpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(JsonPath.parse(response.getBody()).read("$.email", String.class)).isEqualTo(newEmail);
        assertThat(JsonPath.parse(response.getBody()).read("$.password", String.class)).isNull();
    }

    @Test
    @Order(145)
    void updateUserByWrongUser() {
        headers.setBasicAuth("tester", TEST_PASSWORD);
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> entity = restTemplate.exchange(getUrl() + "/5", HttpMethod.GET, httpEntity, UserDto.class);
        assertThat(entity.getStatusCodeValue()).isEqualTo(200);
        final UserDto prepared = entity.getBody();
        final String newEmail = "MrUpdated2@hu.hu";

        if (prepared == null) {
            fail("Non prepared user in DB");
        }

        prepared.setEmail(newEmail);
        headers.setBasicAuth("stranger", "simitar");
        final HttpEntity<UserDto> putHttpEntity = new HttpEntity<>(prepared, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/5", HttpMethod.PUT, putHttpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(403);
    }

    @Test
    @Order(150)
    void updateUserByNull() {
        headers.setBasicAuth("admin", "simitar");
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        UserDto prepared = null;

        final HttpEntity<UserDto> putHttpEntity = new HttpEntity<>(prepared, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/5", HttpMethod.PUT, putHttpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(160)
    void updateUserWithoutId() {
        headers.setBasicAuth("tester", TEST_PASSWORD);
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> entity = restTemplate.exchange(getUrl() + "/5", HttpMethod.GET, httpEntity, UserDto.class);
        final UserDto prepared = entity.getBody();

        if (prepared == null) {
            fail("Non prepared user in DB");
        }

        prepared.setUserId(null);
        final HttpEntity<UserDto> putHttpEntity = new HttpEntity<>(prepared, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/5", HttpMethod.PUT, putHttpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(170)
    void updateUserWithNotEmailString() {
        headers.setBasicAuth("tester", TEST_PASSWORD);
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> entity = restTemplate.exchange(getUrl() + "/5", HttpMethod.GET, httpEntity, UserDto.class);
        final UserDto prepared = entity.getBody();

        if (prepared == null) {
            fail("Non prepared user in DB");
        }

        prepared.setEmail("sdhfnsjd54s654f54sd@");
        final HttpEntity<UserDto> putHttpEntity = new HttpEntity<>(prepared, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/5", HttpMethod.PUT, putHttpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }


    @Test
    @Order(180)
    void updateUserWithoutUsername() {
        headers.setBasicAuth("tester", TEST_PASSWORD);
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> entity = restTemplate.exchange(getUrl() + "/5", HttpMethod.GET, httpEntity, UserDto.class);
        final UserDto prepared = entity.getBody();

        if (prepared == null) {
            fail("Non prepared user in DB");
        }

        prepared.setUsername(null);
        final HttpEntity<UserDto> putHttpEntity = new HttpEntity<>(prepared, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/5", HttpMethod.PUT, putHttpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(190)
    void updateUserWithBlankUsername() {
        headers.setBasicAuth("tester", TEST_PASSWORD);
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> entity = restTemplate.exchange(getUrl() + "/5", HttpMethod.GET, httpEntity, UserDto.class);
        final UserDto prepared = entity.getBody();

        if (prepared == null) {
            fail("Non prepared user in DB");
        }

        prepared.setUsername("");
        final HttpEntity<UserDto> putHttpEntity = new HttpEntity<>(prepared, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrl() + "/5", HttpMethod.PUT, putHttpEntity, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(200)
    void deleteUserByWrongUser() {
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> response = restTemplate.exchange(getUrl() + "/5", HttpMethod.DELETE, httpEntity, UserDto.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(403);
    }

    @Test
    @Order(210)
    void deleteUserWithoutUserId() {
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> response = restTemplate.exchange(getUrl(), HttpMethod.DELETE, httpEntity, UserDto.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(405);
    }

    @Test
    @Order(220)
    void deleteUserWithNonExistingUserId() {
        headers.setBasicAuth("admin", "simitar");
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> response = restTemplate.exchange(getUrl() + "/666", HttpMethod.DELETE, httpEntity, UserDto.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(220)
    void deleteUserByUserSelf() {
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> response = restTemplate.exchange(getUrl() + "/1", HttpMethod.DELETE, httpEntity, UserDto.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    @Order(230)
    void deleteUserByAdmin() {
        headers.setBasicAuth("admin", "simitar");
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        final ResponseEntity<UserDto> response = restTemplate.exchange(getUrl() + "/5", HttpMethod.DELETE, httpEntity, UserDto.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    private String getUrl() {

        return "http://localhost:" + port + "/user";
    }

    private UserDto prepareUserDto() {
        final UserDto dto = new UserDto();
        dto.setUsername("tester");
        dto.setPassword(TEST_PASSWORD);
        dto.setEmail("hu@hu.hu");
        dto.setRoles("ROLE_USER");
        return dto;
    }
}