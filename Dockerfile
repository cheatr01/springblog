FROM openjdk:11
WORKDIR /
ARG PATH_TO_TARGET=target/
ENV PATH_TO_TARGET=$PATH_TO_TARGET
ADD ${PATH_TO_TARGET}/springblog.jar //
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "/springblog.jar"]